From a311dc4ade908452d7920452a18ce411af0f6dd3 Mon Sep 17 00:00:00 2001
From: Riccardo Schirone <sirmy15@gmail.com>
Date: Thu, 17 Jun 2021 16:39:23 +0200
Subject: [PATCH] Check return value of pam_get_item/pam_get_data functions

(cherry picked from commit a22cbf85ed9863ba5c86681db89424747119ef0c)

Resolves: #1973210
---
 src/login/pam_systemd.c | 66 ++++++++++++++++++++++++++++++++++-------
 1 file changed, 55 insertions(+), 11 deletions(-)

diff --git a/src/login/pam_systemd.c b/src/login/pam_systemd.c
index f8bd17eefe..1b643d52ca 100644
--- a/src/login/pam_systemd.c
+++ b/src/login/pam_systemd.c
@@ -705,7 +705,11 @@ _public_ PAM_EXTERN int pam_sm_open_session(
          * "systemd-user" we simply set XDG_RUNTIME_DIR and
          * leave. */
 
-        (void) pam_get_item(handle, PAM_SERVICE, (const void**) &service);
+        r = pam_get_item(handle, PAM_SERVICE, (const void**) &service);
+        if (!IN_SET(r, PAM_BAD_ITEM, PAM_SUCCESS)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM service: %s", pam_strerror(handle, r));
+                return r;
+        }
         if (streq_ptr(service, "systemd-user")) {
                 char rt[STRLEN("/run/user/") + DECIMAL_STR_MAX(uid_t)];
 
@@ -719,10 +723,26 @@ _public_ PAM_EXTERN int pam_sm_open_session(
 
         /* Otherwise, we ask logind to create a session for us */
 
-        (void) pam_get_item(handle, PAM_XDISPLAY, (const void**) &display);
-        (void) pam_get_item(handle, PAM_TTY, (const void**) &tty);
-        (void) pam_get_item(handle, PAM_RUSER, (const void**) &remote_user);
-        (void) pam_get_item(handle, PAM_RHOST, (const void**) &remote_host);
+        r = pam_get_item(handle, PAM_XDISPLAY, (const void**) &display);
+        if (!IN_SET(r, PAM_BAD_ITEM, PAM_SUCCESS)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM XDISPLAY: %s", pam_strerror(handle, r));
+                return r;
+        }
+        r = pam_get_item(handle, PAM_TTY, (const void**) &tty);
+        if (!IN_SET(r, PAM_BAD_ITEM, PAM_SUCCESS)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM TTY: %s", pam_strerror(handle, r));
+                return r;
+        }
+        r = pam_get_item(handle, PAM_RUSER, (const void**) &remote_user);
+        if (!IN_SET(r, PAM_BAD_ITEM, PAM_SUCCESS)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM RUSER: %s", pam_strerror(handle, r));
+                return r;
+        }
+        r = pam_get_item(handle, PAM_RHOST, (const void**) &remote_host);
+        if (!IN_SET(r, PAM_BAD_ITEM, PAM_SUCCESS)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM RHOST: %s", pam_strerror(handle, r));
+                return r;
+        }
 
         seat = getenv_harder(handle, "XDG_SEAT", NULL);
         cvtnr = getenv_harder(handle, "XDG_VTNR", NULL);
@@ -789,11 +809,31 @@ _public_ PAM_EXTERN int pam_sm_open_session(
 
         remote = !isempty(remote_host) && !is_localhost(remote_host);
 
-        (void) pam_get_data(handle, "systemd.memory_max", (const void **)&memory_max);
-        (void) pam_get_data(handle, "systemd.tasks_max",  (const void **)&tasks_max);
-        (void) pam_get_data(handle, "systemd.cpu_weight", (const void **)&cpu_weight);
-        (void) pam_get_data(handle, "systemd.io_weight",  (const void **)&io_weight);
-        (void) pam_get_data(handle, "systemd.runtime_max_sec", (const void **)&runtime_max_sec);
+        r = pam_get_data(handle, "systemd.memory_max", (const void **)&memory_max);
+        if (!IN_SET(r, PAM_SUCCESS, PAM_NO_MODULE_DATA)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM systemd.memory_max data: %s", pam_strerror(handle, r));
+                return r;
+        }
+        r = pam_get_data(handle, "systemd.tasks_max",  (const void **)&tasks_max);
+        if (!IN_SET(r, PAM_SUCCESS, PAM_NO_MODULE_DATA)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM systemd.tasks_max data: %s", pam_strerror(handle, r));
+                return r;
+        }
+        r = pam_get_data(handle, "systemd.cpu_weight", (const void **)&cpu_weight);
+        if (!IN_SET(r, PAM_SUCCESS, PAM_NO_MODULE_DATA)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM systemd.cpu_weight data: %s", pam_strerror(handle, r));
+                return r;
+        }
+        r = pam_get_data(handle, "systemd.io_weight",  (const void **)&io_weight);
+        if (!IN_SET(r, PAM_SUCCESS, PAM_NO_MODULE_DATA)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM systemd.io_weight data: %s", pam_strerror(handle, r));
+                return r;
+        }
+        r = pam_get_data(handle, "systemd.runtime_max_sec", (const void **)&runtime_max_sec);
+        if (!IN_SET(r, PAM_SUCCESS, PAM_NO_MODULE_DATA)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM systemd.runtime_max_sec data: %s", pam_strerror(handle, r));
+                return r;
+        }
 
         /* Talk to logind over the message bus */
 
@@ -996,7 +1036,11 @@ _public_ PAM_EXTERN int pam_sm_close_session(
 
         /* Only release session if it wasn't pre-existing when we
          * tried to create it */
-        (void) pam_get_data(handle, "systemd.existing", &existing);
+        r = pam_get_data(handle, "systemd.existing", &existing);
+        if (!IN_SET(r, PAM_SUCCESS, PAM_NO_MODULE_DATA)) {
+                pam_syslog(handle, LOG_ERR, "Failed to get PAM systemd.existing data: %s", pam_strerror(handle, r));
+                return r;
+        }
 
         id = pam_getenv(handle, "XDG_SESSION_ID");
         if (id && !existing) {
